use bardecoder;
use ctrlc;
use image::{DynamicImage, ImageBuffer};
use std::{
	sync::{
		atomic::{AtomicBool, Ordering},
		mpsc::sync_channel,
		Arc,
	},
	thread,
};
use uvc;

fn frame_to_image(frame: &uvc::Frame) -> Option<DynamicImage> {
	Some(DynamicImage::ImageRgb8(ImageBuffer::from_raw(
		frame.width(),
		frame.height(),
		frame.to_rgb().ok()?.to_bytes().to_vec(),
	)?))
}

fn main() {
	let (send, recv) = sync_channel(0);

	thread::spawn(move || {
		let ctx = uvc::Context::new().expect("Could not create context");
		let dev = ctx
			.find_device(None, None, None)
			.expect("Could not find device");

		let description = dev.description().unwrap();
		eprintln!(
			"Found device: Bus {:03} Device {:03} : ID {:04x}:{:04x} {} ({})",
			dev.bus_number(),
			dev.device_address(),
			description.vendor_id,
			description.product_id,
			description.product.unwrap_or_else(|| "Unknown".to_owned()),
			description
				.manufacturer
				.unwrap_or_else(|| "Unknown".to_owned())
		);

		let devh = dev.open().expect("Could not open device");

		let format = devh
			.get_preferred_format(|x, y| {
				if x.fps <= y.fps && x.width * x.height >= y.width * y.height {
					x
				} else {
					y
				}
			})
			.unwrap();

		eprintln!("Best format found: {:?}", format);
		let mut streamh = devh.get_stream_handle_with_format(format).unwrap();

		let foo: &'static () = &();
		let _stream = streamh
			.start_stream(
				|frame_in, _| {
					if let Some(img) = &frame_to_image(frame_in) {
						if let Some(Ok(result)) = std::panic::catch_unwind(move || {
							bardecoder::default_decoder().decode(img)
						})
						.map_err(|e| {
							if std::env::args().next() == Some(String::from("--debug")) {
								img.save_with_format("qrcam_debug.png", image::ImageFormat::PNG)
									.expect("Cannot save debug image");
							}
							eprintln!("Decoder panicked: {:?}", e);
						})
						.unwrap()
						.get(0)
						{
							println!("{}", result);
							std::process::exit(0);
						}
					}
				},
				foo,
			)
			.unwrap();

		send.send(()).unwrap();
	});

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Error setting Ctrl-C handler");

	while running.load(Ordering::SeqCst) {}

	recv.recv().unwrap();
}
