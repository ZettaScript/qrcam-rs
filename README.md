# QRCam

Simple program to read QRcode from webcam

## Why ?

QRcodes are a very easy and quick way to transfer small text data from paper or cellphone to computer, without any pairing nor radio antenna.

## Use

    qrcam > output

The program ends when it finds a valid QRcode. `stdout` is the output text; errors and debug are thrown to `stderr`.

## Future

* Display the image in a graphic window
* Use less CPU
* Choose between multiple detected QRcodes (currently it selects the first but ordering may be arbitrary)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
